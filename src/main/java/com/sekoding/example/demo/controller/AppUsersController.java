package com.sekoding.example.demo.controller;

import javax.validation.Valid;

import com.sekoding.example.demo.dto.ResponseData;
import com.sekoding.example.demo.models.entities.AppUser;
import com.sekoding.example.demo.services.AppUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/users")
public class AppUsersController {

    @Autowired
    AppUserService appUserService;

    @PostMapping("/register")
    public ResponseEntity<ResponseData<AppUser>> register(@Valid @RequestBody AppUser appUser, Errors errors) {
        ResponseData<AppUser> responseData = new ResponseData<>();

        if (errors.hasErrors()) {
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessage().add((error.getDefaultMessage()));
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        responseData.setStatus(true);
        responseData.setPayload(appUserService.registerAppUser(appUser));
        return ResponseEntity.ok(responseData);

    }
}
