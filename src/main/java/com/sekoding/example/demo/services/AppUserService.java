package com.sekoding.example.demo.services;

import javax.transaction.Transactional;

import com.sekoding.example.demo.models.entities.AppUser;
import com.sekoding.example.demo.models.repos.AppUserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AppUserService implements UserDetailsService {
    @Autowired
    private AppUserRepo appUserRepo;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return appUserRepo.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException(String.format("user with email '%s' not found", username)));

    }

    public AppUser registerAppUser(AppUser user) {
        boolean userExists = appUserRepo.findByUsername(user.getUsername()).isPresent();
        if (userExists) {
            throw new RuntimeException(String.format("user with username '%s' already exist", user.getUsername()));
        }

        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        return appUserRepo.save(user);
    }
}
